<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Offer::create(['name' => 'Offer 1', 'capacity' => 50]);
        \App\Models\Offer::create(['name' => 'Offer 2', 'capacity' => 70]);
        \App\Models\Offer::create(['name' => 'Offer 3', 'capacity' => 10]);
        \App\Models\Offer::create(['name' => 'Offer 4', 'capacity' => 30]);
    }

}
