<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OfferController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/offers', [OfferController::class, 'index'])->name('offers.index');
Route::post('/offers/distribute-requests', [OfferController::class, 'distributeRequests'])->name('offers.distribute-requests');
Route::post('/offers/reset', [OfferController::class, 'resetOffers'])->name('offers.reset');
