<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RequestsUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $offerId;
    public $takenRequests;
    public $capacity;

    public function __construct($offerId, $takenRequests, $capacity)
    {
        $this->offerId = $offerId;
        $this->takenRequests = $takenRequests;
        $this->capacity = $capacity;
    }

    public function broadcastOn()
    {
        return new Channel('requests');
    }
}