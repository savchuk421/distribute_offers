<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use Illuminate\Http\Request;
use Pusher\Pusher;
use Illuminate\Support\Facades\Log;

class OfferController extends Controller
{
    public function index()
    {
        $offers = Offer::all();
        return view('offers.index', compact('offers'));
    }

    public function distributeRequests(Request $request)
    {
        $totalRequests = $request->input('total_requests', 0);

        $offers = Offer::all();
        $totalCapacity = $offers->sum('capacity');

        $addedRequests = [];

        $distributedRequests = 0;

        while ($distributedRequests < $totalRequests) {
            foreach ($offers as $offer) {
                if (!isset($addedRequests[$offer->id]) || $addedRequests[$offer->id] < round($totalRequests * ($offer->capacity / $totalCapacity))) {
                    if ($offer->taken_requests < $offer->capacity) {
                        $offer->taken_requests++;
                        $offer->save();

                        $this->sendPusher('requests.updated', [
                            'offerId' => $offer->id,
                            'name' => $offer->name,
                            'takenRequests' => $offer->taken_requests,
                            'capacity' => $offer->capacity,
                        ]);

                        usleep(200000);

                        if (!isset($addedRequests[$offer->id])) {
                            $addedRequests[$offer->id] = 1;
                        } else {
                            $addedRequests[$offer->id]++;
                        }

                        $distributedRequests++;
                    }
                }
            }
        }

        $updatedOffers = Offer::all();

        return response()->json([
            'message' => 'Requests distributed successfully',
            'offers' => $updatedOffers,
        ]);
    }

    public function resetOffers()
    {
        Offer::query()->update(['taken_requests' => 0]);

        $updatedOffers = Offer::all();

        return response()->json([
            'message' => 'Offers reset successfully',
            'offers' => $updatedOffers,
        ]);
    }

    private function sendPusher($event, $data)
    {
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            [
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'useTLS' => true,
            ]
        );

        $pusher->trigger('offers', $event, $data);
    }
}
