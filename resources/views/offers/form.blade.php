<form action="{{ route('offers.distribute-requests') }}" method="post">
    @csrf

    <label for="total_requests">Total Requests:</label>
    <input type="number" name="total_requests" id="total_requests" min="0">

    <button type="submit">Distribute offers</button>
</form>
