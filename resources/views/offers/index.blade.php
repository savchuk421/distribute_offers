<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css">
    <link rel="stylesheet" href="{{ asset('resources/css/app.css') }}">

    <title>Offers</title>
</head>
<body>
    <h1>Offers</h1>

    <form id="distribute-form">
        @csrf
        <label for="total_requests">Total Requests:</label>
        <input type="number" id="total_requests" name="total_requests" required>
        <button type="submit">Distribute offers</button>
        <button id="reset-offers">Reset Offers</button>
    </form>

    <div id="offers-list">
    @foreach ($offers as $offer)
        <div class="offer" id="offer-{{ $offer->id }}">
            <span class="offer-name">{{ $offer->name }}</span> - 
            <span class="offer-requests" id="offer-requests-{{ $offer->id }}">{{ $offer->taken_requests }}</span> / {{ $offer->capacity }}
            <div class="progress-bar" id="progress-bar-{{ $offer->id }}"></div>
        </div>
    @endforeach
</div>

    </div>
    
    <script>
        document.getElementById('distribute-form').addEventListener('submit', function (e) {
            e.preventDefault();
            NProgress.start(); // Начало работы прогресс-бара

            const totalRequests = document.getElementById('total_requests').value;

            fetch("{{ url('/offers/distribute-requests') }}", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                },
                body: JSON.stringify({
                    total_requests: totalRequests
                }),
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data.message);
                    NProgress.done();
                });
        });

        const pusher = new Pusher('30bbe4330182e7a0e4be', {
            cluster: 'eu',
            encrypted: true,
        });

        const channel = pusher.subscribe('offers');
        channel.bind('requests.updated', function (data) {
            updateSingleOffer(data);
        });

        function updateSingleOffer(data) {
            const offerElement = document.getElementById(`offer-${data.offerId}`);
            const progressBar = document.getElementById(`progress-bar-${data.offerId}`);
            const offerRequests = document.getElementById(`offer-requests-${data.offerId}`);

            if (offerElement) {
                offerRequests.innerText = `${data.takenRequests}`;

                const progress = (data.takenRequests / data.capacity) * 100;
                progressBar.style.width = `${progress}%`;
                
                // Добавляем проценты внутри прогресс-бара
                progressBar.innerText = `${Math.round(progress)}%`;
            }
        }
    </script>
</body>
</html>
